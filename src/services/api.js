import axios from "axios";

const API_KEY = '30a0b495-1c2c-4790-ab75-22e775a60ca1';
const BASE_URL = 'https://content.guardianapis.com/';
const API_AND ='%20AND%20';

export const api = {
    getNews(currentPage = 1, section = null, searchValue = '') {
        let searchParams = '';
        if (searchValue) {
            let array = searchValue.split(' ');
            let i = 0;
            while (i < array.length-1) {
                array.splice(i+1, 0, API_AND);
                i += 2;
            };
            searchParams = `q=${array.join('')}`;
        };

        const sectionUrl = (section) ? `=${section}` : `-`;
        return axios.get(`${BASE_URL}search?section${sectionUrl}&show-fields=thumbnail&api-key=${API_KEY}&page=${currentPage}&${searchParams}`)
            .then(response => response.data)
            .then(data => data.response)
            .then(news => news.results)
    },

    getSections() {
        return axios.get(`${BASE_URL}sections?api-key=${API_KEY}`)
            .then(response => response.data)
            .then(data => data.response)
            .then(sections => sections.results)
    }
};
