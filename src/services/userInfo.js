import axios from "axios"


export const getUserInfo = async () => {
    const response = await axios.get(`https://ipapi.co/json/`);
    const data = await response.data;
    return data;
};