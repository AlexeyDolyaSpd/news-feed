import { useDispatch } from "react-redux";
import { Header } from "./components/Header/Header";
import { NewsList } from "./components/NewsList/NewsList";
import { useEffect } from "react";
import { getUserInfoRequest } from "./store/actions/userInfo.actions";
import { getSectionsRequest } from "./store/actions/news.actions";
import "./App.css";

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUserInfoRequest());
  }, []);
  
  useEffect(() => {
    dispatch(getSectionsRequest());
  }, []);

  return (
    <div className="App">
      <Header />
      <NewsList />
    </div>
  );
}

export default App;
