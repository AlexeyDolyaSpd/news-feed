import React, { useEffect, useState } from "react";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormHelperText from "@mui/material/FormHelperText";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import { useDispatch, useSelector } from "react-redux";

export const NewsSelect = ({ selectSection }) => {
  const [pickedSection, setPickedSection] = useState("");
  const sections = useSelector((state) => state.sections.data);
  const isLoading = useSelector((state) => state.sections.isLoading);
  const dispatch = useDispatch();

  const handleChange = (e) => {
    const section = e.target.value;
    setPickedSection(section);
    selectSection(section);
  };

  return (
    <div>
      {isLoading ? (
        <></>
      ) : (
        <FormControl sx={{ m: 1, minWidth: 120 }}>
          <InputLabel id="demo-simple-select-helper-label">Sections</InputLabel>
          <Select
            labelId="demo-simple-select-helper-label"
            id="demo-simple-select-helper"
            value={pickedSection}
            onChange={handleChange}
          >
            <MenuItem value={""}>
              <em>All</em>
            </MenuItem>
            {sections.map((item) => (
              <MenuItem key={item.id} value={item.id}>
                {item.webTitle}
              </MenuItem>
            ))}
          </Select>
          <FormHelperText>choose news section</FormHelperText>
        </FormControl>
      )}
    </div>
  );
};
