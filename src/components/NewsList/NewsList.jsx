import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { getNewsListRequest, incrementPage } from '../../store/actions/news.actions';
import { NewsItem } from '../NewsItem/NewsItem';
import { NewsSelect } from '../NewsSelect/NewsSelect';
import { SearchForm } from '../SearchForm/SearchForm'
import { Grid } from 'react-loader-spinner';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import ListSubheader from '@mui/material/ListSubheader';
import './style.css';



export const NewsList = () => {
    const [section, setSection] = useState(null);
    const [searchValue, setSearchValue] = useState('');

    const newsList = useSelector(state => state.newsList.data);
    const isLoading = useSelector(state => state.newsList.isLoading);
    const pageCounter = useSelector(state => state.newsList.pageCounter);

    const dispatch = useDispatch();

    useEffect(() => {
            dispatch(getNewsListRequest(pageCounter, section, searchValue));
    }, [pageCounter, section, searchValue]);

    useEffect(() => {
        document.addEventListener("scroll", handleScroll);
        return  () => {
            document.removeEventListener("scroll", handleScroll);
        };
    }, []);

    const handleScroll = (e) => {
        let scroll = e.target.documentElement.scrollHeight - (e.target.documentElement.scrollTop + window.innerHeight);
        if (scroll < 20 && !isLoading) {
            dispatch(incrementPage())
        }
    };

    return (
        <div className="newsListWrapper">
            <ImageList >
                <ImageListItem key="Subheader" cols={2}>
                    <ListSubheader component="div">News</ListSubheader>
                    <SearchForm handleSearch={setSearchValue}/>
                    <NewsSelect selectSection={setSection} />
                </ImageListItem>
                { (newsList.length > 0)
                  ? newsList.map((news) => (
                    <a rel='noreferrer' target='_blank' href={news.webUrl} key={news.id}>
                        <NewsItem
                            url={news.fields?.thumbnail}
                            title={news.webTitle}
                            sectionName={news.sectionName}
                            publicationDate={news.webPublicationDate} />
                    </a>
                    ))
                  :<ImageListItem key="Supheader" cols={2}><ListSubheader component="div">News not found</ListSubheader></ImageListItem>}
            </ImageList>
            {isLoading ? <div className='loader'><Grid color="#00BFFF" height={80} width={80} /></div> : <></>}
        </div>
    );
};
