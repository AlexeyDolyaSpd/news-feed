import React from 'react';
import ImageListItemBar from '@mui/material/ImageListItemBar';
import ImageListItem from '@mui/material/ImageListItem';
import moment from 'moment';

export const NewsItem = ({
    url,
    title,
    sectionName,
    publicationDate
}) => {

    return (
        <ImageListItem title={title} >
        <img
          src={url}
          alt={title}
          loading="lazy"
        />
        <ImageListItemBar
          title={title}
          subtitle={`${moment(publicationDate).format('MMMM Do YYYY')} / ${sectionName}`}
        />
      </ImageListItem>
    );
};
