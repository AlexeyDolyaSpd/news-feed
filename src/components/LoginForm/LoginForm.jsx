import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { useSelector } from "react-redux";
import Alert from "@mui/material/Alert";
import PhoneInputWithCountry from 'react-phone-number-input/react-hook-form';
import IconButton from "@mui/material/IconButton";
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import { yupResolver } from '@hookform/resolvers/yup';
import { formSchema } from "../../utils/formSchema";
import "./style.css";
import 'react-phone-number-input/style.css';


export const LoginForm = () => {

  const countryCode = useSelector(state => state.userInfo.data.countryCode);
  const [isPasswordShown, setIsPasswordShown] = useState(false);
  const [isConfirmPasswordShown, setIsConfirmPasswordShown] = useState(false);

  const {control, register, handleSubmit, reset,formState: { errors }} = useForm({mode: "onBlur", resolver: yupResolver(formSchema)});

  const onSubmit = (data) => {
    delete data.passwordConfirm;
    reset();
    console.log(data);
  }; 
  return (
    <form className="loginForm" onSubmit={handleSubmit(onSubmit)}>
     <div className="formItem">
        <input
          type="text"
          placeholder="First name"
          {...register("firstName")}
        />
        {errors.firstName ? <Alert severity="error">{errors.firstName.message}</Alert>: <></>}
     </div>
     <div className="formItem">
        <input
          type="text"
          placeholder="Last name"
          {...register("lastName")}
        />
        {errors.lastName ? <Alert severity="error">{errors.lastName.message}</Alert> : <></>}
      </div>
      <div className="formItem">
        <input
          type="email"
          placeholder="Email"
          {...register("email")}
        />
        {errors.email ? <Alert severity="error">{errors.email.message}</Alert> : <></>}
      </div>
      <div className="formItem">
          <PhoneInputWithCountry
            international
            countryCallingCodeEditable={false}
            defaultCountry={countryCode}
            placeholder="Phone number"
            name="phoneNumber"
            control={control}
          />
          {errors.phoneNumber ? <Alert severity="error">{'This is not a valid number'}</Alert> : <></>}
      </div>
      <div className="formItem radio">
        <label>
          Male
          <input checked type="radio" value="Male" {...register("Gender")} />
        </label>
        <label>
          Female
          <input type="radio" value="Female" {...register("Gender")} />
        </label>
      </div>
      <div className="formItem">
        <div className="passwordField">
          <input 
            type={isPasswordShown ? "text" : "password"} 
            placeholder="Password" 
            {...register('password')}/>
            <IconButton onClick={() => setIsPasswordShown(!isPasswordShown)} title={!isPasswordShown? 'show password' : 'hide password'} >
                {!isPasswordShown? <RemoveRedEyeIcon/> : <VisibilityOffIcon/>}
            </IconButton>
        </div>
          {errors.password ? <Alert severity="error">{errors.password.message}</Alert> : <></>}
        <div className="passwordField">
          <input
            type={isConfirmPasswordShown ? "text" : "password"}
            placeholder="Confirm password"
            {...register("passwordConfirm")}
          />
          <IconButton onClick={() => setIsConfirmPasswordShown(!isConfirmPasswordShown)} title={!isConfirmPasswordShown? 'show password' : 'hide password'} >
                {!isConfirmPasswordShown? <RemoveRedEyeIcon/> : <VisibilityOffIcon/>}
          </IconButton>
        </div>
        {errors.passwordConfirm ? <Alert severity="error">{errors.passwordConfirm.message}</Alert> : <></>}
      </div>
      <div className="formItem agreement">
        <label>
            <input type="checkbox" {...register("agreement")}/> I accept some shit
        </label>
        {errors.agreement ? <Alert severity="error">{errors.agreement.message}</Alert> : <></>}
      </div>
      <input className="formItem" value='Sign up' type="submit" />
    </form>
  );
};
