import React, { useState } from "react";
import TextField from '@mui/material/TextField';
import SearchIcon from '@mui/icons-material/Search';
import IconButton from "@mui/material/IconButton";
import './style.css';

export const SearchForm = ({handleSearch}) => {
  const [value, setValue] = useState('');

  const handleChange = (e) => {
    setValue(e.target.value.trim())
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    handleSearch(value)
  };

  return (
    <form  onSubmit={(e) => handleSubmit(e)}>
      <div className="searchWrapper">
      <TextField onChange={(e) => handleChange(e)} id="filled-basic" label="Search" variant="filled" ></TextField>
      <IconButton type="submit">
        <SearchIcon/>
      </IconButton>
      </div>
    </form>
  );
};
