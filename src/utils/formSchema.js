import * as Yup from 'yup';
import "yup-phone";

export const formSchema = Yup.object().shape({
    firstName: Yup.string()
      .trim()
      .required('First name is required')
      .matches(/^[A-Za-z]+$/i, 'Is not in correct format'),
    lastName: Yup.string()
      .trim()
      .required('Last name is required')
      .matches(/^[A-Za-z]+$/i, 'Is not in correct format'),
    email: Yup.string()
      .required('Email is required')
      .email('Invalid email format'),
    phoneNumber:Yup.string()
      .phone()
      .required('Phone is required'),
    password: Yup.string()
      .required('Password is required')
      .min(6, 'Password length should be at least 6 characters'),
    passwordConfirm: Yup.string()
      .required('Confirm Password is required')
      .oneOf([Yup.ref('password')], 'Passwords must and should match'),
      agreement: Yup.boolean()
      .oneOf([true],'You need to accept this' )
  });