import { put, takeLatest } from "redux-saga/effects";
import { api } from "../../services/api";
import { getSectionsFail, getSectionsSuccess, GET_SECTIONS_REQUEST } from "../actions/news.actions";

function* sectionsWorker() {
    try {
        const data = yield api.getSections()
        yield put(getSectionsSuccess(data))
    } catch (error) {
        yield put(getSectionsFail(error?.response?.data?.errors))
    }
};

export function* sectionsWatcher() {
    yield takeLatest(GET_SECTIONS_REQUEST, sectionsWorker)
};
