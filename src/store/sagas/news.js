import { put, takeLatest } from "redux-saga/effects";
import { api } from "../../services/api";
import { getNewsListFail, getNewsListSuccess, GET_NEWS_REQUEST } from "../actions/news.actions";

function* allNewsWorker({ payload: { currentPage, section, searchValue } }) {
    try {
        const data = yield api.getNews(currentPage, section, searchValue);
        yield put(getNewsListSuccess(data, currentPage))
    } catch (error) {
        yield put(getNewsListFail(error?.response?.data?.errors))
    }
};

export function* allNewsWatcher() {
    yield takeLatest(GET_NEWS_REQUEST, allNewsWorker)
};
