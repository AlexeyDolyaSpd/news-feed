import { put, takeLatest } from "redux-saga/effects";
import { getUserInfo } from "../../services/userInfo";
import { getUserInfoFail, getUserInfoSuccess, GET_USER_INFO_REQUEST } from "../actions/userInfo.actions";

function* userInfoWorker() {
    try {
        const data = yield getUserInfo()
        yield put(getUserInfoSuccess(data))
    } catch (error) {
        yield put(getUserInfoFail(error?.response?.data?.errors))
    }
};

export function* userInfoWatcher() {
    yield takeLatest(GET_USER_INFO_REQUEST, userInfoWorker)
};

