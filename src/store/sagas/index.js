import { all } from 'redux-saga/effects';
import { allNewsWatcher } from "./news"
import { sectionsWatcher } from "./sections"
import { userInfoWatcher } from './userInfo';


export function* rootSaga(){
    yield all([
        allNewsWatcher(),
        sectionsWatcher(),
        userInfoWatcher()
    ])
}
