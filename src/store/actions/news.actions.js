export const GET_NEWS_REQUEST = 'GET_NEWS_REQUEST';
export const GET_NEWS_SUCCESS = 'GET_NEWS_SUCCESS';
export const GET_NEWS_FAIL = 'GET_NEWS_FAIL';
export const GET_SECTIONS_REQUEST = 'GET_SECTIONS_REQUEST';
export const GET_SECTIONS_SUCCESS = 'GET_SECTIONS_SUCCES';
export const GET_SECTIONS_FAIL = 'GET_SECTIONS_FAIL';
export const INCREMENT_PAGE = 'INCREMENT_PAGE';

export const getNewsListRequest = (currentPage, section, searchValue) => {
    return {
        type: GET_NEWS_REQUEST,
        payload: {
            currentPage,
            section,
            searchValue
        }
    }
};


export const getNewsListSuccess = (data, currentPage) => {
    return {
        type: GET_NEWS_SUCCESS,
        payload:{
            data,
            currentPage
        }
    }
};

export const getNewsListFail = (payload) => {
    return {
        type: GET_NEWS_FAIL,
        payload
    }
};


export const getSectionsRequest = () => {
    return {
        type: GET_SECTIONS_REQUEST
    }
};

export const getSectionsSuccess = (payload) => {
    return {
        type: GET_SECTIONS_SUCCESS,
        payload
    }
};


export const getSectionsFail = (payload) => {
    return {
        type: GET_SECTIONS_FAIL,
        payload
    }
};

export const incrementPage = () => {
    return{
        type:INCREMENT_PAGE
    }
};
