import { GET_NEWS_FAIL } from "./news.actions";

export const GET_USER_INFO_REQUEST = 'GET_USER_INFO_REQUEST ';
export const GET_USER_INFO_SUCCESS = 'GET_USER_INFO_SUCCESS';
export const GET_USER_INFO_FAIL = 'GET_USER_INFO_FAIL';

export const getUserInfoRequest = () => {
    return {
        type:GET_USER_INFO_REQUEST
    }
};

export const getUserInfoSuccess = (payload) => {
    return {
        type: GET_USER_INFO_SUCCESS,
        payload
    }
};

export const getUserInfoFail = (payload) => {
    return {
        type: GET_NEWS_FAIL,
        payload
    }
};