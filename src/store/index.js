import { applyMiddleware, createStore } from "redux";
import { rootReducer } from "./reducers";
import createSagaMiddleware from 'redux-saga';
import {rootSaga} from './sagas'

const sagaMiddleware= createSagaMiddleware();

export const configureStore = () => {
    const store = createStore(
        rootReducer,
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__(
            applyMiddleware(sagaMiddleware)
        )
    )
    sagaMiddleware.run(rootSaga);
    return store;
}