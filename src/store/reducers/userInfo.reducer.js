import {
  GET_USER_INFO_FAIL,
  GET_USER_INFO_REQUEST,
  GET_USER_INFO_SUCCESS,
} from "../actions/userInfo.actions";

const initialState = {
  isLoading: false,
  errors: null,
  data: {
    countryCode: null,
  },
};

export const userInfoReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USER_INFO_REQUEST:
      return {
        ...state,
        isLoading: true,
      };

    case GET_USER_INFO_SUCCESS:
      return {
        ...state,
        isLoading: false,
        data: {
          countryCode: action.payload.country_code,
        },
      };

    case GET_USER_INFO_FAIL:
      return {
        ...state,
        isLoading: false,
        errors: action.payload,
      };

    default:
      return state;
  }
};
