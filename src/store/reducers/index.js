import { combineReducers } from "redux"
import { newsListReducer } from "./newsList.reducer"
import { sectionsReducer } from "./sections.reducer"
import { userInfoReducer } from "./userInfo.reducer";


export const rootReducer = combineReducers({
    newsList: newsListReducer,
    sections: sectionsReducer,
    userInfo: userInfoReducer
});