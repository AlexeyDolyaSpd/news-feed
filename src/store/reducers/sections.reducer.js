import {
  GET_SECTIONS_SUCCESS,
  GET_SECTIONS_FAIL,
  GET_SECTIONS_REQUEST,
} from "../actions/news.actions";

const initialState = {
  data: [],
  isLoading: false,
  errors: null,
};

export const sectionsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_SECTIONS_REQUEST:
      return {
        ...state,
        isLoading: true,
      };

    case GET_SECTIONS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };

    case GET_SECTIONS_FAIL:
      return {
        ...state,
        isLoading: false,
        errors: action.payload,
      };

    default:
      return state;
  }
};
