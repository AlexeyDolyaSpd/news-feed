import {
  GET_NEWS_FAIL,
  GET_NEWS_REQUEST,
  GET_NEWS_SUCCESS,
  INCREMENT_PAGE,
} from "../actions/news.actions";

const initialState = {
  data: [],
  isLoading: false,
  errors: null,
  pageCounter: 1,
  currentPage: 1,
};

export const newsListReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_NEWS_REQUEST:
      return {
        ...state,
        isLoading: true,
      };

    case GET_NEWS_SUCCESS:
      if (state.pageCounter === state.currentPage) {
        return {
          ...state,
          pageCounter: 1,
          currentPage: 1,
          data: action.payload.data,
          isLoading: false,
        };
      } else {
        return {
          ...state,
          data: [...state.data, ...action.payload.data],
          isLoading: false,
          currentPage: state.pageCounter,
        };
      }

    case INCREMENT_PAGE:
      return {
        ...state,
        pageCounter: state.pageCounter + 1,
      };

    case GET_NEWS_FAIL:
      return {
        ...state,
        isLoading: false,
        errors: action.payload,
      };

    default:
      return state;
  }
};
